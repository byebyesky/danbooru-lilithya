const Token = 'NTI2OTU1MDI2MjUwMjAzMTU3.DwRlCA.pTu8tPn_w2NkCXmCFrLFdGU7qM0'
const Discord = require('discord.js');
const client = new Discord.Client();
const Danbooru = require('danbooru');
const booru = new Danbooru();
require('console-stamp')(console, '[HH:MM:ss]');

const HELPSTRING =  "```!booru {-t} {Tag1 Tag2} - Starts sending pictures (Tags are optional). -t shows tags.\n"+
                    "!stop - Stops sending pictures\n"+
                    "!help - Shows this menu\n"+
                    "!delete [number] - deletes all messages from Lilithya within [number] of posts\n"+
                    "!play - Play guess the tag(not implemented)```";

async function getDanbooruPost(tags) {
  let post;
  let url;

  await booru.posts({ random: true, tags: tags, limit: 10 }).then(posts => {
    if(posts.length) {
	let index = Math.floor(Math.random() * posts.length);
  post = posts[index];
	url = booru.url(post.file_url);

	while(url.href == 'https://danbooru.donmai.us/' || url.href.includes('.zip')) {
		index = Math.floor(Math.random() * posts.length);
          post = posts[index];
          url = booru.url(post.file_url);
  }
    }
    else { throw 'Cannot find any posts!'; }
  })
    return post;
}

function prepareTags(args) {
  let tags = "";
  if(args.length > 2) {throw 'You can only specify up to 2 tags!';}
  else if(args.length < 2) { tags = 'order:random '; }
  for(let i = 0; i < args.length; i++) { tags += args[i] + " "; }
  return tags;
}

function stopBot(msg, channelArray) {
  let index = channelArray.findIndex(user => user.channel === msg.channel);
  if(index === -1) {
    msg.channel.send('Bot is not running!');
  } else {channelArray[index].stop = true;}
}

function prepareDiscordString(post, showTags){
  let discordString = "";
  if(showTags){
    if(post.tag_string_general) {discordString += `Tags: ${post.tag_string_general}\n\n`}
    if(post.tag_string_character) {discordString +=`Character: ${post.tag_string_character}\n\n`}
    if(post.tag_string_artist) {discordString += `Artist: ${post.tag_string_artist}\n\n`}
    if(post.tag_string_copyright) {discordString += `Copyright: ${post.tag_string_copyright}`}
    if(discordString !== "") {discordString = "`" + discordString + "`\n"}
  }
  discordString += booru.url(post.file_url).href;
  return discordString;
}

function postURL(msg, userTags, user) {
    getDanbooruPost(userTags)
    .then(post => { 
      msg.channel.send(prepareDiscordString(post, user.showTags));
    })
	  .catch(error =>	{
          console.log('Error: Cannot find ' + args);
	  			msg.channel.send(error + ' Stopping...');
	  			stopBot(msg, channelArray);
        })
}

function handleGame(msg) {
  
}

let channelArray = [];
let activeGameArray = [];
const sendInterval = 5000;
client.on('ready', () => { console.log(`Logged in as ${client.user.tag}!`); });
client.on('error', console.error);
client.on('message', msg => {
  const args = msg.content.trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  if (command === '!booru') {
    if(channelArray.findIndex(user => user.channel === msg.channel) === -1) {
      let channelDetails
      if(msg.channel.type === 'dm') { 
	      channelDetails = msg.author.tag + '...';
	    } else {
        channelDetails = msg.channel.name + '@' + msg.guild.name + ' by: ' + msg.author.tag + '...'
      }

      let tagsEnabled = false;
      let tagParam = args.indexOf("-t");
      if(tagParam !== -1){
        args.splice(tagParam, 1);
        tagsEnabled = true;
      }

      let user = {
        channel: msg.channel,
        stop: false,
        showTags: tagsEnabled
      };

      console.log("Starting to send to: " + channelDetails);
      console.log('Searching for: ' + args);

      channelArray.push(user);
      
      let userTags;
      try {
        userTags = prepareTags(args);
      } catch (error) { msg.channel.send(error + ' Stopping...');
        stopBot(msg, channelArray);
      }
      
      postURL(msg, userTags, user);
      
      let timer = setInterval(function() {
        if(channelArray[channelArray.findIndex(user => user.channel === msg.channel)].stop === true) {
            console.log("Stopping to send to: " + channelDetails);
            clearInterval(timer);
            channelArray.splice(channelArray.findIndex(user => user.channel === msg.channel), 1);
        } else {
          postURL(msg, userTags, user);
        }
      }, sendInterval)
    } else {
      msg.channel.send("Bot is already running!");
    }
  };

  if (command === '!play') {
    
  }

  if (command === '!stop') {
    stopBot(msg, channelArray);
  }

  if (command === '!delete' && msg.channel.type === 'dm') {
    number = parseInt(args[0])
    if(isNaN(number)) {
	msg.channel.send("Please specify a valid number of messages to delete!"); }
    else if(number <= 100) {
      msg.channel.fetchMessages({ limit: args[0] })
	.then(messages => messages.forEach(item => { 
		if(item.author.id === client.user.id) {item.delete()}
	}))
 	.catch(console.error);
    } else {
      msg.channel.send('Please specify a number <= 100');
    }
  }

  if (command === '!help') {
    msg.channel.send(HELPSTRING);
  }

});

client.login(Token);